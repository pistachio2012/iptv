package com.ranpeng.pistachio;

import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import org.chromium.content.browser.JavascriptInterface;
import tv.danmaku.ijk.media.example.widget.media.IjkVideoView;

public class IptvMediaPlayer {
  private static final String TAG = "IptvMediaPlayer";
  private IjkVideoView mVideo = null;
  private Handler mHandler = null;

  public IptvMediaPlayer(IjkVideoView video) {
    mVideo = video;
    mHandler = new Handler(Looper.getMainLooper());
  }

  @JavascriptInterface
  public void playFromStart() {
    Log.i(TAG, "playFromStart");
    mHandler.post(new Runnable() {
      @Override
      public void run() {
        mVideo.setVideoPath("http://www.w3school.com.cn/example/html5/mov_bbb.mp4");
        mVideo.start();
      }
    });
  }
}


