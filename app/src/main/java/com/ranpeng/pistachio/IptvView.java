package com.ranpeng.pistachio;

import android.app.Activity;

import org.chromium.WebView;

public class IptvView extends WebView {
    public static void InitAndConfig(Activity activity) {
        WebView.Init(activity);
        WebView.Config("enable-devtools", "");              //开启调试
        WebView.Config("enable-logging", "");               //开启日志
    }

    private final String TAG = "IptvView";
    public IptvView(final Activity activity, WebView.StartupCallback callback) {
        super(activity, callback);
    }
}

/***
 *  1.iptv.aar是基于浏览器的中间件
 *  2.assets/init.js是一段js代码在网页加载前会被执行，
 *    这样可以在这段js中，将IPTV自定义的js对象调用转成java注册类调用，如MediaPlayer
 *  3.在onCreatedTab中注册js对象类，如IptvMediaPlayer
 *  4.中间件对省份比较依赖，如果有什么事情可以发邮件。
 ***/

