package com.ranpeng.pistachio;

import android.app.Activity;
import android.content.Intent;
import android.content.DialogInterface;
import android.content.res.AssetManager;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.TableLayout;
import android.widget.Toast;
import java.io.InputStream;

import org.chromium.content.browser.JavascriptInterface;
import org.chromium.WebView;
import tv.danmaku.ijk.media.example.widget.media.IjkVideoView;

public class MainActivity extends Activity implements WebView.StartupCallback {
  private static final String TAG = "MainActivity";
  private String mStartupUrl;

  private IptvView mIptv = null;
  private IjkVideoView mVideo = null;
  @Override
  protected void onCreate(final Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

    mVideo = new IjkVideoView(this);

    IptvView.InitAndConfig(this);
    mIptv = new IptvView(this, this);
    mIptv.setFocusable(true);
    mIptv.requestFocus();
    mVideo.addView(mIptv);

    TableLayout table = new TableLayout(this);
    FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(
        ViewGroup.LayoutParams.WRAP_CONTENT, // 宽度
        ViewGroup.LayoutParams.WRAP_CONTENT); // 高度
    layoutParams.gravity = Gravity.END | Gravity.TOP; // 结合END（右对齐）和TOP（顶部对齐）
    mVideo.setHudView(table);
    mVideo.addView(table, layoutParams);

    setContentView(mVideo);
    mStartupUrl = getUrlFromIntent(getIntent());
  }

  @Override
  public void onCreateTab(int id) {
    mIptv.addInitJavascriptString(getInitJSString());
    mIptv.addJavascriptInterface(new IptvMediaPlayer(mVideo), "IptvMediaPlayer");
  }

  @Override
  public void onDestroyTab(int id) {
    Log.i(TAG, "onDestroyTab:" + id);
  }

  @Override
  public void onSuccess() {
    String shellUrl;
    if (!TextUtils.isEmpty(mStartupUrl)) {
      shellUrl = mStartupUrl;
    } else {
      shellUrl = "https://www.baidu.com";
    }
    mIptv.loadUrl(shellUrl);
  }

  @Override
  public void onFailure() {
    Log.e(TAG, "ContentView initialization failed.");
    Toast.makeText(MainActivity.this, "ContentView initialization failed.", Toast.LENGTH_SHORT).show();
    finish();
  }

  @Override
  protected void onNewIntent(Intent intent) {
    String url = getUrlFromIntent(intent);
    if (!TextUtils.isEmpty(url))
      mIptv.loadUrl(url);
  }

  @Override
  protected void onStart() {
    super.onStart();
    mIptv.onStart();
  }

  @Override
  protected void onDestroy() {
    mIptv.onDestroy();
    super.onDestroy();
  }

  private static String getUrlFromIntent(Intent intent) {
    return intent != null ? intent.getDataString() : null;
  }

  private String getInitJSString() {
    String script = "";
    try {
      InputStream is = getAssets().open("init.js", AssetManager.ACCESS_STREAMING);
      StringBuffer sb = new StringBuffer();
      byte[] str = new byte[256];
      int ret = is.read(str, 0, 256);
      while(ret > 0) {
        sb.append(new String(str, 0, ret, "UTF-8"));
        ret = is.read(str, 0, 256);
      }
      script = sb.toString();
    } catch (Exception e) {
      Log.e(TAG, "open helper error:" + e);
      script = "";
    }
    return script;
  }

  @Override
  public boolean dispatchKeyEvent(KeyEvent e) {
    Log.i(TAG, "dispatchKeyEvent:" + e);
//    if (mIptv != null)
//      mIptv.dispatchKeyEvent(e);
    return super.dispatchKeyEvent(e);
  }
}

