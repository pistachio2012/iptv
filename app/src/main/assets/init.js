/*** MediaPlayer object ***/
var MediaPlayer = function() {
  this.getNativePlayerInstanceID = function(){
    return window.IptvMediaPlayer.getNativePlayerInstanceID();
  };
  this.setVideoDisplayMode = function(mode){
    return window.IptvMediaPlayer.setVideoDisplayMode(mode);
  };
  this.setNativeUIFlag = function(mode){
    return window.IptvMediaPlayer.setNativeUIFlag(mode);
  };
  this.setVolume = function(mode){
    return window.IptvMediaPlayer.setVolume(mode);
  };
  this.setMuteUIFlag = function(mode){
    return window.IptvMediaPlayer.setMuteUIFlag(mode);
  };
  this.setAudioVolumeUIFlag = function(mode){
    return window.IptvMediaPlayer.setAudioVolumeUIFlag(mode);
  };
  this.setAudioTrackUIFlag = function(mode){
    return window.IptvMediaPlayer.setAudioTrackUIFlag(mode);
  };
  this.setProgressBarUIFlag = function(mode){
    return window.IptvMediaPlayer.setProgressBarUIFlag(mode);
  };
  this.setChannelNoUIFlag = function(mode){
    return window.IptvMediaPlayer.setChannelNoUIFlag(mode);
  };
  this.setMuteFlag = function(mode){
    return window.IptvMediaPlayer.setMuteFlag(mode);
  };
  this.setAllowTrickmodeFlag = function(mode){
    return window.IptvMediaPlayer.setAllowTrickmodeFlag(mode);
  };
  this.setVideoDisplayArea = function(x, y, length, heigh){
    if(typeof x == 'string')  {x = parseInt(x)  }  
    if(typeof y == 'string')  {y = parseInt(y)  }  
    if(typeof length == 'string')  {length = parseInt(length)  }  
    if(typeof heigh == 'string')  {heigh= parseInt(heigh)  }  
    return window.IptvMediaPlayer.setVideoDisplayArea(x, y, length, heigh);
  };
  this.setCycleFlag = function(x){
    return window.IptvMediaPlayer.setCycleFlag(x);
  };
  this.refreshVideoDisplay = function(){
    return window.IptvMediaPlayer.refreshVideoDisplay();
  };
  this.joinChannel = function(mode){
    return window.IptvMediaPlayer.joinChannel(mode);
  };
  this.bindNativePlayerInstance = function(mode){
    return window.IptvMediaPlayer.bindNativePlayerInstance(mode);
  };
  this.leaveChannel = function(){
    return window.IptvMediaPlayer.leaveChannel();
  };
  this.getVolume = function(){
    return window.IptvMediaPlayer.getVolume();
  };
  this.gotoEnd = function(){
    return window.IptvMediaPlayer.gotoEnd();
  };
  this.gotoStart = function(){
    return window.IptvMediaPlayer.gotoStart();
  };
  this.stop = function(){
    return window.IptvMediaPlayer.stop();
  };
  this.setRandomFlag = function(x){
    return window.IptvMediaPlayer.setRandomFlag(x);
  };
  this.getChannelNum = function(){
    return window.IptvMediaPlayer.getChannelNum();
  };
  this.initMediaPlayer = function( instanceId, playListFlag, videoDisplayMode, height, width, left, top, muteFlag, useNativeUIFlag, subtitleFlag, videoAlpha, cycleFlag, randomFlag, autoDelFlag){
    return window.IptvMediaPlayer.initMediaPlayer(instanceId, playListFlag, videoDisplayMode, height, width, left, top, muteFlag, useNativeUIFlag, subtitleFlag, videoAlpha, cycleFlag, randomFlag, autoDelFlag);
  };
  this.initMediaBasePlayer = function( instanceId, playListFlag, videoDisplayMode, height, width, left, top, muteFlag, useNativeUIFlag, subtitleFlag, videoAlpha, cycleFlag, randomFlag, autoDelFlag){
    return window.IptvMediaPlayer.initMediaPlayer(instanceId, playListFlag, videoDisplayMode, height, width, left, top, muteFlag, useNativeUIFlag, subtitleFlag, videoAlpha, cycleFlag, randomFlag, autoDelFlag);
  };
  this.setSingleMedia = function(mediaStr){
    return window.IptvMediaPlayer.setSingleMedia(mediaStr);
  };
  this.playFromStart = function(){
    return window.IptvMediaPlayer.playFromStart();
  };
  this.getNativeUIFlag = function(){
    return window.IptvMediaPlayer.getNativeUIFlag();
  };
  this.getChannelNoUIFlag = function(){
    return window.IptvMediaPlayer.getChannelNoUIFlag();
  };
  this.playByTime = function(type,timestamp,speed){
    return window.IptvMediaPlayer.playByTime(type,timestamp,speed);
  };
  this.resume = function(){
    return window.IptvMediaPlayer.resume();
  };
  this.pause = function(){
    return window.IptvMediaPlayer.pause();
  };
  this.getCurrentPlayTime = function(){
    return window.IptvMediaPlayer.getCurrentPlayTime();
  };
  this.fastForward = function(speed){
    return window.IptvMediaPlayer.fastForward(speed);
  };
  this.fastRewind = function(speed){
    return window.IptvMediaPlayer.fastRewind(speed);
  };
  this.setSingleOrPlaylistMode = function(mode){
    return window.IptvMediaPlayer.setSingleOrPlaylistMode(mode);
  };
  this.switchAudioChannel = function(){
    return window.IptvMediaPlayer.switchAudioChannel();
  };
  this.getMediaDuration = function(){
    return window.IptvMediaPlayer.getMediaDuration();
  };
  this.getMuteUIFlag = function(){
    return window.IptvMediaPlayer.getMuteUIFlag();
  };
  this.getPlaybackMode = function(){
    return window.IptvMediaPlayer.getPlaybackMode();
  };
  this.getMuteFlag = function(){
    return window.IptvMediaPlayer.getMuteFlag();
  };
  this.getCurrentAudioChannel = function(){
    return window.IptvMediaPlayer.getCurrentAudioChannel();
  };
  this.sendVendorSpecificCommand = function(mode){
    return window.IptvMediaPlayer.sendVendorSpecificCommand(mode);
  };
  this.releaseMediaPlayer = function(mode){
    return window.IptvMediaPlayer.releaseMediaPlayer(mode);
  };
  this.setVideoAlpha = function(mode){
    return window.IptvMediaPlayer.setVideoAlpha(mode);
  };
  this.set = function(){
    return window.IptvMediaPlayer.set();
  };
  this.stop = function(){
    return window.IptvMediaPlayer.stop();
  };
}

